%%
clear
clc
close all

train = csvread('id_train.csv',1);
test = csvread('sample_submission4.csv',1);
N_train = size(train,1);
N_test = size(test,1);

train_images = cell(N_train,1);
test_images = cell(N_test,1);


%%
%load data
width = 64;
hight = 64;
c_train = zeros(N_train, width*hight);
for ii = 1:N_train
    path = strcat('roof_images/', num2str(train(ii,1)), '.jpg');
    train_images{ii} = imresize(rgb2gray(imread(path)),[width,hight]);
    c_train(ii,:) =  train_images{ii}(:);
end

c_test = zeros(N_test, width*hight);
for ii = 1:N_test
    path = strcat('roof_images/', num2str(test(ii,1)), '.jpg');
    test_images{ii} = imresize(rgb2gray(imread(path)),[width,hight]);
    c_test(ii,:) =  test_images{ii}(:);
end
%%
%feature extraction
feat_train_hog = zeros(N_train,1764);
feat_test_hog = zeros(N_test,1764);
for ii = 1:N_train
    feat_train_hog(ii,:) = extractHOGFeatures(train_images{ii});
end

for ii = 1:N_test
    feat_test_hog(ii,:) = extractHOGFeatures(test_images{ii});
end


%%
%PCA

[loadings, scores, variance] = pca(feat_train_hog);
cumvar = cumsum(variance)/sum(variance);
plot(1:100:length(cumvar),cumvar(1:100:length(cumvar)),'b*');

%%
%keep first 500
keep  = 1000;
needed_loadings = loadings(:,1:keep);
mean_values = mean(c_train);
x_zip = scores(:,1:keep);
x_rec = x_zip * needed_loadings' + repmat(mean_values,N_train,1);
x_test = (c_test - repmat(mean(c_test),N_test,1)) * needed_loadings;

%%
%svd
[U,S,V] = svd(c_train); 
eigs = diag(S);
plot(1:size(eigs),eigs,'ro');
x_zip = U*S;
x_zip = x_zip(:,1:100);


%%
%reconstruction
train_images_rec = cell(N_train,1);
for ii = 1:N_train
    train_images_rec{ii} = uint8(reshape(x_rec(ii,:),[width,hight]));
end

%%
%knn cv
y = train(:,2);
ii=1;

learner = @(X,t) fitcknn(X,t,'NumNeighbors',20);
predictor = @(model,X) predict(model,X);
scorer =  @(t,pred) sum(t ~= pred)/length(t);

[mean_error, ~, scores] = cross_validate(feat_train_hog,y,learner,predictor,scorer,5);
%%
submission = [train(:,1), scores];
dlmwrite('knn_20_train.csv', submission, 'precision', '%i');

%%
%knn cv
y = train(:,2);
ii=1;
for kk = [10:3:25] 
    display(['k =', num2str(kk)]);
    knn_model = fitcknn(feat_train_hog,y,'NumNeighbors',kk);
    cvmodel = crossval(knn_model,'kfold',10);
    cvError(ii) = kfoldLoss(cvmodel);
    display(['error =', num2str(cvError(ii))]);
    ii = ii+1;
end
plot([10:3:25] ,cvError,'bo');
%%
%knn train predict
y = train(:,2);
knn_model = fitcknn(feat_train_hog,y,'NumNeighbors',20);
[pred, scores] = predict(knn_model,feat_test_hog);

%make sumbission
submission = [test(:,1), scores];
dlmwrite('knn_20_test.csv', submission, 'precision', '%i');
