function [mean_error, errors, scores] = cross_validate(X,t, learner, predictor, scorer, k)
    N = size(X,1);
    %shuffler = randperm(N);
    %X = X(shuffler,:);
    %t = t(shuffler);
    scores = zeros(N,4);
    l = round(N/k);
    errors = zeros(1,k);
    for ii = 1:k
        
        %build the training and validation sets
        start = (ii-1)*l+1;
        stop = min(ii*l,N);
        
        disp(['Xvalid start =' num2str(start) ' stop=' num2str(stop)]);
        
        ind = zeros(1,N);
        ind(start:stop) = 1;
        ind = logical(ind);
        nind = logical(1-ind);
        
        X_train = X(nind,:);
        X_valid = X(ind,:);
        t_train = t(nind);
        t_valid = t(ind);
        
        model = learner(X_train,t_train);
        [pred, scores(start:stop,:)] = predictor(model,X_valid);
        
        errors(ii) = scorer(t_valid,pred);
    end
    mean_error = mean(errors);
end