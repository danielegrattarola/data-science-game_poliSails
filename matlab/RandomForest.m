function [oobPrediction, prediction, probabilities] = randForest(train, test, target, numTrees)
forest = TreeBagger(numTrees,train,target,'OOBPrediction','On',...
    'Method','classification', 'NumPrint', 10);
fprintf('Done train trees');

oobPrediction = forest.oobPredict();
fprintf('Done out of bag prediction');

[prediction, probabilities] = forest.predict(test);
end