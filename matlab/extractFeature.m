function feature = extractFeature(Id, folder, numPoint)
len = length(Id);
feature = zeros(len, numPoint*5);
for ii = 1:len
    fprintf(sprintf('%d / %d\n', ii, len));
    image = rgb2gray(imread(sprintf('%s\\%s.jpg', folder, num2str(Id(ii)))));
    point = detectSURFFeatures(image);
    stronges = point.selectStrongest(numPoint);
    
    scale = zeros(numPoint,1);
    sc = double(stronges.Scale);
    scale(1:length(sc),1) = sc;
    
    orientation = zeros(numPoint,1);
    or = double(stronges.Orientation);
    orientation(1:length(or),1) = or;

    location = zeros(1,2*numPoint);
    lo = [double(stronges.Location(:,1))', double(stronges.Location(:,2))'];
    location(1,1:length(lo)) = lo;

    laplacian = zeros(numPoint,1);
    la = double(stronges.SignOfLaplacian);
    laplacian(1:length(la),1) = la;
    
    feature(ii,:) = double([scale', orientation', ...
        location, laplacian']);
end
