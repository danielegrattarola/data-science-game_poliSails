[Id, label] = csvread('id_train.csv');
[Id1, label1] = csvread('sample_submission4.csv');

CopyTrain;
CopyTest;
trainSet = [ imageSet(fullfile('Train', '1')), ...
            imageSet(fullfile('Train', '2')), ...
            imageSet(fullfile('Train', '3')), ...
            imageSet(fullfile('Train', '4'))];
testSet = imageSet(fullfile('test'));
bag = bagOfFeatures(trainSet);
categoryClassifier = trainImageCategoryClassifier(trainSet, bag);
confMatrix = evaluate(categoryClassifier, testSet);
prediction = predict(categoryClassifier, testSet);

csvwrite('submission.csv', num2str(Id1));