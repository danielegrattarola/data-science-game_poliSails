clc;
clear;

train = csvread('id_train.csv',1);
Id = train(:,1);
label = train(:,2);
train = extractFeature(Id, 'roof_images', 20);
fprintf('Done loading train set');

test = csvread('sample_submission4.csv',1);
Id1 = test(:,1);
test = extractFeature(Id1, 'roof_images', 20);
fprintf('Done loading test set');

[outOfBag, predictionForest, probabilities] = randForest(train, test, label, 1000);

table = table([Id1 str2num(cell2mat(predictionForest))]);
table = ['Id,label', table];
writetable(table, 'randSubmission.csv', 'WriteVariableNames', false);
fprintf('Done writing submission file');