MATLAB scripts
==============

How to use
--------------
1. Copy "roof_images" inside the MATLAB scripts (or change the path inside Main.m)
2. Open MATLAB and add folder and subfolder (right click on the matlab folder of the repo)
3. (Optional) make parameter tuning such as number of trees and num of SURF strogest parameter
4. Write "main" inside the MATLAB console