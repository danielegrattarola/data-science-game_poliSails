%%
clear
clc
close all

train = csvread('id_train.csv',1);
test = csvread('sample_submission4.csv',1);
N_train = size(train,1);
N_test = size(test,1);

train_images = cell(N_train,1);
test_images = cell(N_test,1);


%%
%load data
width = 64;
hight = 64;
c_train = zeros(N_train*2, width*hight);
for ii = 1:N_train
    path = strcat('roof_images/', num2str(train(ii,1)), '.jpg');
    train_images{ii} = imresize(rgb2gray(imread(path)),[width,hight]);
    c_train(ii,:) =  train_images{ii}(:);
    temp = imrotate(train_images{ii},180);
    c_train(ii+N_train,:) = temp(:);
end

c_test = zeros(N_test*2, width*hight);
for ii = 1:N_test
    path = strcat('roof_images/', num2str(test(ii,1)), '.jpg');
    test_images{ii} = imresize(rgb2gray(imread(path)),[width,hight]);
    c_test(ii,:) =  test_images{ii}(:);
    temp = imrotate(test_images{ii},180);
    c_test(ii+N_test,:) = temp(:);
end
%%
%feature extraction
feat_train_hog = zeros(N_train*2,1764);
feat_test_hog = zeros(N_test,1764);
for ii = 1:N_train
    feat_train_hog(2*ii-1,:) = extractHOGFeatures(train_images{ii});
    feat_train_hog(2*ii,:) = extractHOGFeatures(imrotate(train_images{ii},180));
end

for ii = 1:N_test
    feat_test_hog(ii,:) = extractHOGFeatures(test_images{ii});
end

%%
y = zeros(2*N_train,1);
for ii = 1:N_train
    y(2*ii-1) = train(ii,2);
    y(2*ii) = train(ii,2);
end

tempSvm = templateSVM('Standardize',0,'KernelFunction','polynomial','PolynomialOrder',3 , 'Cost', [0,5;5,0]);
learner = @(X,t) fitcecoc(X,t,'Learners',tempSvm,'Verbose',2,'Coding','onevsall','FitPosterior',1);
predictor = @(model,X) predict(model,X);
scorer =  @(t,pred) sum(t ~= pred)/length(t);

[mean_error, ~, scores] = cross_validate(feat_train_hog,y,learner,predictor,scorer,5);

submission = scores;
dlmwrite('svm_poly_train_rot.csv', submission, 'precision', '%i');

%%
tempSvm = templateSVM('Standardize',0,'KernelFunction','polynomial','PolynomialOrder',3 , 'Cost', [0,5;5,0]);
svm_model = fitcecoc(feat_train_hog,y,'Learners',tempSvm,'Verbose',2,'Coding','onevsall','FitPosterior',1);
[pred,~,~,scores] = predict(svm_model,feat_test_hog);

%%
submission = scores;
dlmwrite('svm_poly_test_rot.csv', submission, 'precision', '%i');
