%% Loading data
clear;
clc;
tic;

train = csvread('id_train.csv',1);
test = csvread('sample_submission4.csv',1);
N_train = size(train,1);
N_test = size(test,1);

train_images = cell(N_train,1);
train_images_rot = cell(N_train,1);
test_images = cell(N_test,1);
labels = train(:,2);

toc;
%% Load images
tic;
width = 90;
hight = 90;
K = 3;
L = 9;
c_train = zeros(N_train, width*hight);
c_train_rot = zeros(N_train, width*hight);
parfor ii = 1:N_train
    fprintf('%d/%d\n', ii, N_train);
    path = strcat('roof_images/', num2str(train(ii,1)), '.jpg');
    image = imresize(rgb2gray(imread(path)),[width,hight]);
    train_images{ii} = image_kmeans(image, K, L);
    c_train(ii,:) =  train_images{ii}(:);
    train_images_rot{ii} = image_kmeans(imrotate(image, 180), K, L);
    c_train_rot(ii,:) =  train_images_rot{ii}(:);
end

c_test = zeros(N_test, width*hight);
parfor ii = 1:N_test
    fprintf('%d/%d\n', ii, N_test);
    path = strcat('roof_images/', num2str(test(ii,1)), '.jpg');
    test_images{ii} = image_kmeans(imresize(rgb2gray(imread(path)),[width,hight]), K, L);
    c_test(ii,:) =  test_images{ii}(:);
end

toc;
%% Random Forest
tic;
forest = TreeBagger(1000, [c_train; c_train_rot], [labels; labels], 'OOBPrediction','On',...
    'Method', 'classification', 'NumPrint', 10);
fprintf('Done train trees\n');

[oobPrediction, oobProbablities] = forest.oobPredict();
fprintf('Done out of bag prediction\n');

[prediction, probabilities] = forest.predict(c_test);
toc;
%% Write results
submission = [test(:,1), str2num(cell2mat(prediction))];
dlmwrite('forest_1000_kmeans.csv', submission, 'precision', '%i');

train_rot_ids = cell(N_train, 1);
for ii=1:N_train
    train_rot_ids{ii} = sprintf('%d_rot',train(ii,1));
end

a_train = zeros(2*N_train, 4);

a_train(1:2:end,:) = oobProbablities(1:8000,:);
a_train(2:2:end,:) = oobProbablities(8001:end,:);

dlmwrite('forest_train.csv', a_train);

a_test = [test(:,1), probabilities];
dlmwrite('forest_test.csv', a_test);