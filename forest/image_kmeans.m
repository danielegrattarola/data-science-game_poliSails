function [I_re] = image_kmeans(Img, K, L)
Img2D_rows=size(Img,1);
Img2D_cols=size(Img,2);

r1=rem(Img2D_rows,sqrt(L));
r2=rem(Img2D_cols,sqrt(L));
Img1=zeros(Img2D_rows+r1,Img2D_cols+r2);
if length(size(Img))==3
	 for i=1:3
        % -----------------------------------------------------------------
        % The condition instructions below is to check for the dimensions of the
        % image and pad the rows/columns accordingly.
        Img1(1:Img2D_rows,1:Img2D_cols)=Img(:,:,i);
        if r1~=0
            Pad_rows=Img(end,:,i);
            for j=1:r1
                Pad_rows(j,:)=Pad_rows(1,:);
            end
            Img1(Img2D_rows+1:end,1:Img2D_cols)=Pad_rows;
        end
        if r1~=0 && r2~=0
            Pad_cols=Img1(:,Img2D_cols);
            for j=1:r2
                Pad_cols(:,j)=Pad_cols(:,1);
            end
            Img1(1:end,Img2D_cols+1:end)=Pad_cols;
        elseif r2~=0
            Pad_cols=Img(:,Img2D_cols);
            for j=1:sqrt(L)-r2
                Pad_cols(:,j)=Pad_cols(:,1);
            end
            Img1(1:Img2D_rows,Img2D_cols+1:end)=Pad_cols;
        end
        % -----------------------------------------------------------------
        if i==1
            I_re=zeros(size(Img1,1),size(Img1,2),3);
        end
        I_re(:,:,i)=Kmeans_Pre_Post(Img1,L,K);  % Kmeans_Pre_Post is a function for re-arranging 
		% the pixels the image into Input vectors to be used for k-means
		% clustering and reconstructing the Image from the Output Vectors.
   end
    I_re=I_re(1:end-r1,1:end-r2,1:3);
else
    % ---------------------------------------------------------------------
    % The "if-elseif-end" condition below is to check for the dimensions of the
    % image and pad the rows/columns accordingly.
    Img1(1:Img2D_rows,1:Img2D_cols)=Img;
    if r1~=0
      Pad_rows=Img(end,:);
      for j=1:r1
          Pad_rows(j,:)=Pad_rows(1,:);
      end
      Img1(1:Img2D_rows,1:Img2D_cols)=Img;
      Img1(Img2D_rows+1:end,1:Img2D_cols)=Pad_rows;
    end
    if r1~=0 && r2~=0
      Pad_cols=Img1(:,Img2D_cols);
      for j=1:r2
          Pad_cols(:,j)=Pad_cols(:,1);
      end
      Img1(1:end,Img2D_cols+1:end)=Pad_cols;
    elseif r2~=0
      Pad_cols=Img(:,Img2D_cols);
      for j=1:sqrt(L)-r2
          Pad_cols(:,j)=Pad_cols(:,1);
      end
      Img1(1:Img2D_rows,1:Img2D_cols)=Img;
      Img1(1:Img2D_rows,Img2D_cols+1:end)=Pad_cols;
    end
    % ---------------------------------------------------------------------
    I_re=Kmeans_Pre_Post(Img1,L,K);
end
I_re = uint8(I_re);
clear Img1;
end