from keras.models import Sequential
from keras.layers import *
from keras.optimizers import *
from keras.regularizers import *


class DCNetwork:
	def __init__(self, input_shape, output_size, learning_rate = None, dropout_prob = 0.5, load_path = None, logger = None):
		self.logger = logger
		self.model = Sequential()

		self.model.add(BatchNormalization(axis=3, input_shape=input_shape))

		self.model.add(Convolution2D(32, 3, 3, border_mode='same', W_regularizer=l2(), dim_ordering='tf'))
		self.model.add(Activation('relu'))
		self.model.add(BatchNormalization(axis=3))

		self.model.add(Convolution2D(32, 3, 3, border_mode='same', W_regularizer=l2(), dim_ordering='tf'))
		self.model.add(Activation('relu'))
		self.model.add(BatchNormalization(axis=3))

		self.model.add(MaxPooling2D(pool_size=(3,3), dim_ordering='tf'))

		self.model.add(Convolution2D(64, 2, 2, border_mode='same', W_regularizer=l2(), dim_ordering='tf'))
		self.model.add(Activation('relu'))
		self.model.add(BatchNormalization(axis=3))

		self.model.add(Convolution2D(64, 2, 2, border_mode='same', W_regularizer=l2(), dim_ordering='tf'))
		self.model.add(Activation('relu'))
		self.model.add(BatchNormalization(axis=3))

		self.model.add(MaxPooling2D(pool_size=(3,3), dim_ordering='tf'))

		self.model.add((Flatten()))

		self.model.add(Dense(1024))
		self.model.add(Activation('relu'))
		self.model.add(Dropout(dropout_prob))
		self.model.add(BatchNormalization(mode=1))

		self.model.add(Dense(512))
		self.model.add(Activation('relu'))
		self.model.add(Dropout(dropout_prob))
		self.model.add(BatchNormalization(mode=1))

		self.model.add(Dense(256))
		self.model.add(Activation('relu'))
		self.model.add(Dropout(dropout_prob))
		self.model.add(BatchNormalization(mode=1))

		self.model.add(Dense(output_size))
		self.model.add(Activation('softmax'))


		self.optimizer = Adam() if learning_rate is None else SGD(lr=learning_rate)
		if load_path is not None:
			self.load(load_path)
		self.model.compile(optimizer=self.optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

		print self.model.metrics_names

	def train(self, x, t):
		x = np.asarray(x)
		t = np.asarray(t)
		return self.model.train_on_batch(x,t)

	def predict(self, x):
		# Feed input to the model, return predictions
		x = np.asarray(x)
		return self.model.predict_on_batch(x)

	def test(self, x, t):
		x = np.asarray(x)
		t = np.asarray(t)
		return self.model.test_on_batch(x,t)

	def save(self, filename = None):
		# Save the model weights to disk
		print 'Saving...'
		self.model.save_weights(self.logger.save_file if filename is None else self.logger.path + filename)

	def load(self, path):
		# Load the model weights from path
		print 'Loading...'
		self.model.load_weights(path)





