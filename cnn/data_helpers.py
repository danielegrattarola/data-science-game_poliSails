import random
import numpy as np
from PIL import Image

data_path = '../data/'
image_path = data_path + 'roof_images/'

def parse_csv(filename, nb_classes):
	# Returns IDs and labels of the given csv
	ids = []
	labels = []
	try:
		f = open(filename, 'r')
		for line in f.readlines()[1:]:
			_id, label = line.rstrip().split(',')
			ids.append(_id)
			labels.append(np.asarray([1 if i == int(label) - 1 else 0 for i in range(nb_classes)]))
		return zip(ids, labels)
	except IOError:
		print 'data_helpers:parse_csv Error while opening file:', filename

def batch_iterator(batch_size, nb_epochs, csv_filename, image_size = (100, 100), nb_classes = 4, shuffle = True, validation = True, logger = None):
	# Generates a batch iterator for the images.
	# Batches of length batch_size are randomly created iterating over the dataset nb_epochs times.
	# Set batch size to 'all' if you want to use the whole dataset as batch.
	# Returns IDs as strings, images and labels as numpy arrays
	data = parse_csv(data_path + csv_filename, nb_classes)
	random.shuffle(data)
	images = []

	if validation:
		# Save validation data
		valid_data = data[:batch_size]
		data = data[batch_size:]
		images[:] = []  # Empty the list to free up memory
		ids, labels = zip(*valid_data)
		for _id in ids:
			image = Image.open(image_path + _id + '.jpg')
			image = image.resize(image_size, Image.ANTIALIAS)
			images.append(np.asarray(image))
		yield zip(ids, images, labels)

	data_size = len(data)
	batch_size = data_size if batch_size == 'all' else batch_size
	nb_batches_in_epoch = int(data_size/batch_size) + (1 if data_size % batch_size else 0)

	if logger is not None:
		logger.log('Total number of iterations: ' + str(nb_batches_in_epoch * nb_epochs))

	for epoch in range(nb_epochs):
		if shuffle:
			random.shuffle(data) # Shuffle data at each epoch
		for batch_idx in range(nb_batches_in_epoch):
			images[:] = [] # Empty the list to free up memory
			batch_data = data[batch_idx * batch_size : min((batch_idx + 1) * batch_size, data_size)]
			ids, labels = zip(*batch_data)
			for _id in ids:
				image = Image.open(image_path + _id + '.jpg')
				image = image.resize(image_size, Image.ANTIALIAS)
				images.append(np.asarray(image))
			yield zip(ids, images, labels)

def k_fold_iterator(batch_size, nb_epochs, k, csv_filename, image_size = (100, 100), nb_classes = 4, shuffle = True, logger = None):
	data = parse_csv(data_path + csv_filename, nb_classes)
	datasets = []
	images = []

	data_size = len(data)
	k = k if k>0 else 1 # Prevent division by 0 or negative number
	fold_size = data_size / k if data_size >=k else data_size # Prevent taking zero samples

	# Split the data k times
	for i in range(k):
		datasets.append({})
		datasets[i]['valid'] = data[i*fold_size : min((i+1)*fold_size, data_size)]
		datasets[i]['data'] = data[:i*fold_size] + data[min((i+1)*fold_size, data_size):]

	# Run through the k folds
	for dataset in datasets:
		# Yield the training samples
		nb_batches_in_epoch = int(len(dataset['data']) / batch_size) + (1 if len(dataset['data']) % batch_size else 0)
		if logger is not None:
			logger.log('Fold ' + str(datasets.index(dataset) + 1) + ' of ' + str(len(datasets)) + '.')
			logger.log('Total number of iterations: ' + str(nb_batches_in_epoch * nb_epochs))
		must_validate = False
		must_reset = True
		for epoch in range(nb_epochs):
			if shuffle:
				random.shuffle(dataset['data'])  # Shuffle data at each epoch
			for batch_idx in range(nb_batches_in_epoch):
				images[:] = []  # Empty the list to free up memory
				batch_data = dataset['data'][batch_idx * batch_size: min((batch_idx + 1) * batch_size, len(dataset['data']))]
				ids, labels = zip(*batch_data)
				for _id in ids:
					image = Image.open(image_path + _id + '.jpg')
					image = image.resize(image_size, Image.ANTIALIAS)
					images.append(np.asarray(image))
				yield zip(ids, images, labels), must_validate, must_reset
				must_reset = False

		# Yield validation data
		must_validate = True
		nb_batches_in_epoch = int(len(dataset['valid']) / batch_size) + (1 if len(dataset['valid']) % batch_size else 0)
		for batch_idx in range(nb_batches_in_epoch):
			images[:] = []  # Empty the list to free up memory
			batch_data = dataset['valid'][batch_idx * batch_size: min((batch_idx + 1) * batch_size, len(dataset['valid']))]
			ids, labels = zip(*batch_data)
			for _id in ids:
				image = Image.open(image_path + _id + '.jpg')
				image = image.resize(image_size, Image.ANTIALIAS)
				images.append(np.asarray(image))
			yield zip(ids, images, labels), must_validate, must_reset


