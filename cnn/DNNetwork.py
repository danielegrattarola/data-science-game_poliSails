from keras.models import Sequential
from keras.layers import *
from keras.optimizers import *
from keras.regularizers import l2


class DNNetwork:
	def __init__(self, input_shape, output_size, learning_rate = None, dropout_prob = 0.5, load_path = None, logger = None):
		self.logger = logger
		self.model = Sequential()

		self.model.add(Flatten(input_shape=input_shape))

		self.model.add(Dense(1024, W_regularizer=l2(), b_regularizer=l2())) # In: 1600 - Out: 1024
		self.model.add(Activation('relu'))
		self.model.add(Dense(512, W_regularizer=l2(), b_regularizer=l2()))  # In: 1600 - Out: 1024
		self.model.add(Activation('relu'))

		self.model.add(Dense(output_size)) # In: 256 - Out: 4
		self.model.add(Activation('softmax'))


		self.optimizer = Adam() if learning_rate is None else SGD(lr=learning_rate)
		if load_path is not None:
			self.load(load_path)
		self.model.compile(optimizer=self.optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

		print self.model.metrics_names

	def train(self, x, t):
		x = np.asarray(x)
		t = np.asarray(t)
		return self.model.train_on_batch(x,t)

	def predict(self, x):
		# Feed input to the model, return predictions
		x = np.asarray(x)
		return self.model.predict_on_batch(x)

	def test(self, x, t):
		x = np.asarray(x)
		t = np.asarray(t)
		return self.model.test_on_batch(x,t)

	def save(self):
		# Save the model weights to disk
		print 'Saving...'
		self.model.save_weights(self.logger.path + 'DNN.h5')

	def load(self, path):
		# Load the model weights from path
		print 'Loading...'
		self.model.load_weights(path)
