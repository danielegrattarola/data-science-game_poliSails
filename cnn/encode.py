import data_helpers as dh
from DCAutoencoder import DCAutoencoder
from Logger import Logger

ae_input_shape = (90,90,3)
ae_nb_classes = 4
ae_dropout_prob = 0.2
ae_load_path = 'best_autoencoder.h5'
ae_batch_size = 64

logger = Logger()

AE = DCAutoencoder(
	ae_input_shape,
	dropout_prob=ae_dropout_prob,
	load_path=ae_load_path,
	logger=logger
)

batches = dh.batch_iterator(
		ae_batch_size,
		1,
		'id_all.csv',
		image_size=ae_input_shape[:2],
		nb_classes=ae_nb_classes,
		logger=logger
	)

idx = 0
for batch in batches:
		idx += 1
		print 'Encoding batch', idx
		ids, images, labels = zip(*batch)
		encoded = AE.encode(images)
		outfile = open('encoded_all.csv', 'w')
		for id_, enc in zip(ids, encoded):
			outfile.write(str(id_) + ',' + ','.join(str(e) for e in enc.flatten()) + '\n')
		outfile.close()