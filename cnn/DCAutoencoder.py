from keras.models import Model
from keras.layers import *
from keras.optimizers import *


class DCAutoencoder:
	def __init__(self, input_shape, learning_rate = None, dropout_prob = 0.5, load_path = None, logger = None):
		self.logger = logger

		# Input layer
		self.inputs = Input(shape=input_shape)

		# Encoding layers
		# Convolution
		self.encoded = Convolution2D(32, 3, 3, border_mode='same', dim_ordering='tf')(self.inputs)
		self.encoded = Activation('relu')(self.encoded)
		self.encoded = Dropout(dropout_prob)(self.encoded)
		self.encoded = BatchNormalization(axis=3)(self.encoded)

		# Convolution
		self.encoded = Convolution2D(32, 3, 3, border_mode='same', dim_ordering='tf')(self.encoded)
		self.encoded = Activation('relu')(self.encoded)
		self.encoded = Dropout(dropout_prob)(self.encoded)
		self.encoded = BatchNormalization(axis=3)(self.encoded)

		# Pooling
		self.encoded = MaxPooling2D(pool_size=(3, 3), dim_ordering='tf')(self.encoded)

		# Convolution
		self.encoded = Convolution2D(64, 2, 2, border_mode='same', dim_ordering='tf')(self.encoded)
		self.encoded = Activation('relu')(self.encoded)
		self.encoded = Dropout(dropout_prob)(self.encoded)
		self.encoded = BatchNormalization(axis=3)(self.encoded)

		# Convolution
		self.encoded = Convolution2D(64, 2, 2, border_mode='same', dim_ordering='tf')(self.encoded)
		self.encoded = Activation('relu')(self.encoded)
		self.encoded = Dropout(dropout_prob)(self.encoded)
		self.encoded = BatchNormalization(axis=3)(self.encoded)

		# Pooling
		self.encoded = MaxPooling2D(pool_size=(3, 3), dim_ordering='tf') (self.encoded) # Out: 64x10x10

		# Convolution
		self.encoded = Convolution2D(64, 2, 2, border_mode='same', dim_ordering='tf')(self.encoded)
		self.encoded = Activation('relu')(self.encoded)
		self.encoded = Dropout(dropout_prob)(self.encoded)
		self.encoded = BatchNormalization(axis=3)(self.encoded)

		# Convolution
		self.encoded = Convolution2D(64, 2, 2, border_mode='same', dim_ordering='tf')(self.encoded)
		self.encoded = Activation('relu')(self.encoded)
		self.encoded = Dropout(dropout_prob)(self.encoded)
		self.encoded = BatchNormalization(axis=3)(self.encoded)

		# Pooling
		self.encoded = MaxPooling2D(pool_size=(2, 2), dim_ordering='tf')(self.encoded) # Out: 5x5x64

		# Decoding layers
		# Convolution
		self.decoded = Convolution2D(64, 2, 2, border_mode='same', dim_ordering='tf')(self.encoded)
		self.decoded = Activation('relu')(self.decoded)
		self.decoded = Dropout(dropout_prob)(self.decoded)
		self.decoded = BatchNormalization(axis=3)(self.decoded)

		# Convolution
		self.decoded = Convolution2D(64, 2, 2, border_mode='same', dim_ordering='tf')(self.decoded)
		self.decoded = Activation('relu')(self.decoded)
		self.decoded = Dropout(dropout_prob)(self.decoded)
		self.decoded = BatchNormalization(axis=3)(self.decoded)

		# Upsampling
		self.decoded = UpSampling2D(size=(2, 2), dim_ordering='tf')(self.decoded)
		self.decoded = BatchNormalization(axis=3)(self.decoded)

		# Convolution
		self.decoded = Convolution2D(64, 2, 2, border_mode='same', dim_ordering='tf')(self.decoded)
		self.decoded = Activation('relu')(self.decoded)
		self.decoded = Dropout(dropout_prob)(self.decoded)
		self.decoded = BatchNormalization(axis=3)(self.decoded)

		# Convolution
		self.decoded = Convolution2D(64, 2, 2, border_mode='same', dim_ordering='tf')(self.decoded)
		self.decoded = Activation('relu')(self.decoded)
		self.decoded = Dropout(dropout_prob)(self.decoded)
		self.decoded = BatchNormalization(axis=3)(self.decoded)

		# Upsampling
		self.decoded = UpSampling2D(size=(3, 3), dim_ordering='tf')(self.decoded)
		self.decoded = BatchNormalization(axis=3)(self.decoded)

		# Convolution
		self.decoded = Convolution2D(32, 3, 3, border_mode='same', dim_ordering='tf')(self.decoded)
		self.decoded = Activation('relu')(self.decoded)
		self.decoded = Dropout(dropout_prob)(self.decoded)
		self.decoded = BatchNormalization(axis=3)(self.decoded)

		# Convolution
		self.decoded = Convolution2D(32, 3, 3, border_mode='same', dim_ordering='tf')(self.decoded)
		self.decoded = Activation('relu')(self.decoded)
		self.decoded = Dropout(dropout_prob)(self.decoded)
		self.decoded = BatchNormalization(axis=3)(self.decoded)

		# Upsampling
		self.decoded = UpSampling2D(size=(3, 3), dim_ordering='tf')(self.decoded)
		self.decoded = BatchNormalization(axis=3)(self.decoded)

		# Output layer
		self.outputs = Convolution2D(3,3,3, border_mode='same', dim_ordering='tf')(self.decoded)
		self.outputs = Activation('sigmoid')(self.outputs)

		# Models
		self.autoencoder = Model(input=self.inputs, output=self.outputs)
		self.encoder = Model(input=self.inputs, output=self.encoded)

		self.optimizer = Adam() if learning_rate is None else SGD(lr=learning_rate)
		if load_path is not None:
			self.load(load_path)
		self.autoencoder.compile(optimizer=self.optimizer, loss='binary_crossentropy', metrics=['accuracy'])

		# Save the architecture
		with open(self.logger.path + 'model.json', 'w') as f:
			f.write(self.autoencoder.to_json())
			f.close()

	def train(self, x):
		x = np.asarray(x).astype('float32') / 255 # Normalize pixels in 0-1 range
		return self.autoencoder.train_on_batch(x,x)

	def encode(self, x):
		# Feed input to the model, return encoded images
		x = np.asarray(x).astype('float32') / 255 # Normalize pixels in 0-1 range
		return self.encoder.predict_on_batch(x)

	def predict(self, x):
		# Feed input to the model, return encoded and re-decoded images
		x = np.asarray(x).astype('float32') / 255 # Normalize pixels in 0-1 range
		return self.autoencoder.predict_on_batch(x)

	def test(self, x):
		x = np.asarray(x).astype('float32') / 255 # Normalize pixels in 0-1 range
		return self.autoencoder.test_on_batch(x,x)

	def save(self, filename = None):
		# Save the model weights to disk
		self.logger.log('Saving autoencoder...')
		self.autoencoder.save_weights(self.logger.path + ('autoencoder.h5' if filename is None else filename))

	def load(self, path):
		# Load the model weights from path
		self.logger.log('Loading autoencoder...')
		self.autoencoder.load_weights(path)
