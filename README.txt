# Data Science Game - Team PoliSails - Politecnico di Milano - Italy

Our method was to use a two-layer model with a SVM, a KNN classifier, a deep neural network and a random tree forest at first layer and an xgboost learner at the second layer.

### SVM 
The SVM has been trained on the HOG (Histogram of gradients) features extracted from the original training set with the original images and the same images rotated of 180�. A polynomial kernel of degree 3 has been used.  Kernel parameters are selected in 5 fold cv.

#### KNN
The KNN classifier has been trained on the same features of SVM, using k=20 neighbors. K is selected in 5 fold cv.

#### CNN
The deep convolutional neural networks takes as input a 90x90 RGB image and outputs a 4 dimensional vector containing the probabilities that the image belongs to each of the 4 classes. 
It has 4 convolutional layers and 2 max-pooling layers, followed by 3 dense layers (all with ReLU activations) and has a 4-class softmax output layer.
It was trained with an Adam optimizer to minimize a categorical cross-entropy loss, on a dataset composed of the original images and their 180� rotation. 

### RTF
Another method used for the first level was a Decision Tree Bagging (also called Random Tree Forest).
To train this algorithm we used the same augmented dataset. We preproccesed the images using k-means (k = 3) to compress the images to 8100 features.

### XGBOOST
The second layer is made of an xgboost learner trained on the probability estimations on the training set provided by the first layer and obtained in 5 fold cross validation. Parameter tuning of xgboost is performed in 5 fold cv.

### Dependencies 
The codebase is split between MATLAB, R and python.
The pyhton codebase is based on the Keras framework and also uses numpy, PIL and h5py.

